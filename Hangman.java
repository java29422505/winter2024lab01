import java.util.Scanner;
public class Hangman{

	public static boolean isLetterInWord(String [] word, char c) {
		
		boolean letter= false;
		
	for (int i= 0; i< word.length; i++){
		
		if (word[i] == c){
			
			letter =true;
		}
	}
	return letter;
}
	public static char toUpperCase(char c) {
		
		if ( c >= 'a' && c <= 'z'){
			
			c = ((char(c-32)));
		}
		
		return c;
	}
	
	public static void printWords( boolean[] letter, boolean [] guesses){
		
		for (int i=0; i< letter.lenght; i++){
			
			if (guesses [toUpperCase(letter[i]){
				
				System.out.println(letter[i]);
			}
			else {
				system.out.print("_");
			}
		}
		
	}
	
	public static  void runGame(String word){
		boolean letter0 = false;
		boolean letter1= false;
		boolean letter2 = false;
		boolean letter3 = false;
		int numberOfGuess = 0;
	
		
		while(numberOfGuess < 6 && !(letter0 && letter1 && letter2 && letter3)){

			Scanner reader = new Scanner (System.in);
			
			System.out.println("Guess a letter");
			char guess = reader.nextLine().charAt(0);
			guess =toUpperCase(guess);
			
			if(isLetterInWord(word, guess)==0){
				letter0=true;
			}
			if(isLetterInWord(word, guess)==1){
				letter1=true;
			}
			if(isLetterInWord(word, guess)==2){
				letter2=true;
			}
			if(isLetterInWord(word, guess)==3){
				letter3=true;
			}
            if(isLetterInWord(word, guess)==-1){
				numberOfGuess++;
			}
			
			printWork(word, letter0, letter1, letter2, letter3);
		}
		if (numberOfGuess==6){
		System.out.println("Oops! Better luck next time :)" );
		}
		if(letter0 && letter1 && letter2 && letter3){
		System.out.println("Congrats! You got it :)");
		}
	}
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
	    runGame(word);	
	}
}